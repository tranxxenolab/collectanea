import glob, os, uuid
import unicodedata
import re
import subprocess
from subprocess import Popen, PIPE
import shutil
import time
import random
import urllib

import pymongo
import boto3
from botocore.exceptions import ClientError
import pymongo
from bson.objectid import ObjectId

import youtube_dl
import yt_dlp

from app.database import DB
from app.keys import BOTO_CFG
from app.log import logger

DB.init()
linode_client = boto3.client( 's3', **BOTO_CFG)
BUCKET="tranxxenolab-collectanea"
BUCKET_URL="https://" + BUCKET + ".eu-central-1.linodeobjects.com"

final_filename = ""


def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')

class MyLogger(object):
    def debug(self, msg):
        logger.debug(msg)

    def warning(self, msg):
        logger.warn(msg)

    def error(self, msg):
        logger.error(msg)



if __name__ == "__main__":
    results = DB.find("items", {"downloaded": False})

    filename = ""
    def my_hook(d):
        if d['status'] == 'finished':
            filename = d["filename"]
            logger.debug(filename)
            logger.debug('Done downloading, now converting ...')

    ydl_opts = {
        'outtmpl': '/tmp/collectanea_%(title)s-%(id)s.%(ext)s',
        'logger': MyLogger(),
        'progress_hooks': [my_hook],
    }

    for item in results:
        ID = str(item["_id"])
        print(item)
        if (item["type"] == "video"):
            with yt_dlp.YoutubeDL(ydl_opts) as ydl:
                ydl.download([item["source"]])
           
            # Assume that there is just one file with this prefix
            # TODO this is brittle but should work
            files = glob.glob("/tmp/collectanea_*")
            f = files[0]
            f_shortened = f.split("collectanea_")[1]
    
            uuidPrefix = uuid.uuid4().hex[0:8]
            try:
                logger.info("Trying to upload '{}' to collectanea...".format(f_shortened))
                response = linode_client.upload_file(f, BUCKET, item["type"] + "s/" + uuidPrefix + "-" + f_shortened)
            except ClientError as e:
                logger.error(e)
                next
            item["originalFilename"] = f_shortened
            item["uniqueFilename"] = uuidPrefix + "-" + f_shortened
            item["downloaded"] = True
            item["URL"] = BUCKET_URL + "/videos/" + uuidPrefix + "-" + f_shortened
            results = DB.update("items", ID, item)
            if (results):
                logger.info("Success")
            else:
                logger.error("Error")
    
            os.remove(f)
        elif (item["type"] == "image"):
            URL = item["sourcePage"]
            print("URL!!!! " + URL)
            filename = URL.split("/")[-1]
            processedFilename = filename.split("?")[0]

            location = os.path.join("/tmp/")
            logger.debug(location)
            args = ['wget', '--header="Accept: text/html"', '--user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36 Edg/90.0.818.46"', '-P', location, URL]
            #args = ['wget', '-P', location, URL]
            p = subprocess.Popen(args)
            p.communicate()

            uuidPrefix = uuid.uuid4().hex[0:8]
            try:
                logger.info("Trying to upload '{}' to collectanea...".format(filename))
                print(urllib.parse.unquote(filename))
                response = linode_client.upload_file(os.path.join(location, urllib.parse.unquote(filename)), BUCKET, item["type"] + "s/" + uuidPrefix + "-" + urllib.parse.unquote(processedFilename))
            except ClientError as e:
                logger.error(e)
                next
            except FileNotFoundError as e:
                logger.error(e)
                next
            item["originalFilename"] = urllib.parse.unquote(processedFilename)
            item["uniqueFilename"] = uuidPrefix + "-" + urllib.parse.unquote(processedFilename)
            item["downloaded"] = True
            item["URL"] = BUCKET_URL + "/images/" + uuidPrefix + "-" + urllib.parse.unquote(processedFilename)
            results = DB.update("items", ID, item)
            if (results):
                logger.info("Success")
            else:
                logger.error("Error")
    
            os.remove(os.path.join(location, urllib.parse.unquote(filename)))
   
        elif (item["type"] == "webpage"):
            URL = item["source"]
            title = item["title"]
            title_slug = slugify(title)
            logger.debug(title_slug)

            location = os.path.join("/tmp", title_slug)
            logger.debug(location)
            #args = ['wget', '-E', '-k', '-p', '-H', '--header="Accept: text/html"', '--user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36 Edg/90.0.818.46"', '--page-requisites',   '--convert-links', '-P', location, URL]
            args = ['wget', '-E', '-k', '-p', '-H', '--page-requisites', '--convert-links', '-P', location, URL]
            p = subprocess.Popen(args)
            #print(subprocess.list2cmdline(args))
            p.communicate()

            # TODO: hack to skip sites where wget is not allowed, for some
            # reason, even with a user-agent and header
            try:
                shutil.make_archive(location, "zip", location)
            except FileNotFoundError:
                continue
            zipfile = location + ".zip"

            uuidPrefix = uuid.uuid4().hex[0:8]
            try:
                logger.info("Trying to upload '{}' to collectanea...".format(zipfile))
                response = linode_client.upload_file(zipfile, BUCKET, item["type"] + "s/" + uuidPrefix + "-" + title_slug + ".zip")
            except ClientError as e:
                logger.error(e)
                next
            item["originalFilename"] = title_slug + ".zip"
            item["uniqueFilename"] = uuidPrefix + "-" + title_slug + ".zip"
            item["downloaded"] = True
            item["URL"] = BUCKET_URL + "/webpages/" + uuidPrefix + "-" + title_slug + ".zip"
            results = DB.update("items", ID, item)
            if (results):
                logger.info("Success")
            else:
                logger.error("Error")
    
            os.remove(zipfile)
            shutil.rmtree(location)

        # Sleep before next loop
        sleep_interval = random.randint(30, 120)
        logger.info("Sleeping for {}".format(sleep_interval))
        time.sleep(sleep_interval)

