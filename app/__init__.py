from flask import Flask
from flask_bootstrap import Bootstrap
from flaskext.markdown import Markdown

from app.database import DB

from app.keys import secret_key

def create_app():
    app = Flask(__name__)
    app.secret_key = secret_key
    DB.init()
    Bootstrap(app)
    Markdown(app)
    register_blueprints(app)
    return app 

def register_blueprints(app):
    from app.main import bp as main_bp
    app.register_blueprint(main_bp)
