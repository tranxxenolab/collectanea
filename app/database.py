import pymongo
from bson.objectid import ObjectId
from bson.json_util import dumps

from app.log import logger

class DB(object):
    URI = "mongodb://127.0.0.1:27017"

    @staticmethod
    def init():
        client = pymongo.MongoClient(DB.URI)
        DB.DATABASE = client['collectanea']

    @staticmethod
    def insert(collection, data):
        return DB.DATABASE[collection].insert(data)

    @staticmethod
    def update(collection, ID, data):
        return DB.DATABASE[collection].update({"_id": ObjectId(ID)}, data)

    @staticmethod
    def find_one(collection, query):
        return DB.DATABASE[collection].find_one(query)

    @staticmethod
    def find_all(collection):
        return DB.DATABASE[collection].find()

    @staticmethod
    def find_text(collection, search_string, projection = None, limit = 5):
        # See https://gist.github.com/cpatrick/5719077
        sort = [('_txtscr', {'$meta':'textScore'})]
        if projection is not None:
            results = DB.find_and_project(collection, {"$text": {"$search": search_string}}, projection = projection, limit = limit, sort = sort)
        else:
            results = DB.find(collection, {"$text": {"$search": search_string}}, limit = limit, sort = sort)
        return dumps(results)

    @staticmethod
    def findByID(collection, ID):
        results = DB.find(collection, {"_id": ObjectId(ID)}, limit = 1)
        try:
            return results[0]
        except:
            # TODO raise an exception
            logger.error("Received more than one result for search by ID...this shouldn't happen!")
            return None

    @staticmethod
    def findByTag(collection, tagName, limit = 100, sort = None):
        query = { "tags": { "$in": [tagName] } }
        return DB.find(collection, query, limit = limit, sort = sort)

    @staticmethod
    def find(collection, query, limit = 100, sort = None):
        if sort is None:
            return DB.DATABASE[collection].find(query).limit(limit)
        else:
            return DB.DATABASE[collection].find(query).limit(limit).sort(sort)

    @staticmethod
    def find_and_project(collection, query, projection, limit = 100, sort = None):
        if sort is None:
            return DB.DATABASE[collection].find(query, projection).limit(limit)
        else:
            return DB.DATABASE[collection].find(query, projection).limit(limit).sort(sort)
