import json, datetime, uuid

from functools import wraps

from flask import render_template, request, redirect, flash, jsonify, session, url_for
import boto3
from botocore.exceptions import ClientError
import pymongo
from bson.objectid import ObjectId

from app.main import bp  # noqa
from app.models.items import Item, BaseForm, ConceptItemForm, TemporalityItemForm, DailyTarotItemForm, NoteItemForm, ImageItemForm, VideoItemForm, SoundItemForm, FileItemForm, ProtocolItemForm, ExperimentItemForm, WebpageItemForm, CyclesItemForm, LettersItemForm, LoginForm
from app.database import DB
from app.keys import BOTO_CFG
from app.log import logger

import bcrypt

DB.init()
linode_client = boto3.client( 's3', **BOTO_CFG)
BUCKET="tranxxenolab-collectanea"
BUCKET_URL="https://" + BUCKET + ".eu-central-1.linodeobjects.com"

ties = ["isDependentOn", "isConnectedTo", "isResultOf", "hasConcept", "hasTemporality", "hasReference"]
ties.sort()

item_types = ["cycles", "daily_tarot", "concept", "temporality", "note", "image", "video", "sound", "file", "protocol", "experiment", "webpage", "letters", "all"]
"""
formMapping = {
    "cycles": CyclesItemForm(),
    "daily_tarot": DailyTarotItemForm(),
    "concept": ConceptItemForm(),
    "temporality": TemporalityItemForm(),
    "note": NoteItemForm(),
    "image": ImageItemForm(),
    "video": VideoItemForm(),
    "sound": SoundItemForm(),
    "file": FileItemForm(),
    "protocol": ProtocolItemForm(),
    "experiment": ExperimentItemForm(),
    "webpage": WebpageItemForm(),
}
"""

def require_login(func):
    @wraps(func)
    def wrapper_func(*args, **kwargs):
        if ("username" in session):
            return func(*args, **kwargs)
        else:
            print("HERE!!!")
            flash("You must be logged in.", "error")
            return render_template('login.html', title = "Login", baseForm = BaseForm(), loginForm = LoginForm())
            #return redirect(url_for("login"))

    return wrapper_func

def check_login():
    if ("username" not in session):
        flash("You must be logged in.", "error")
        return redirect(url_for("main.login"))
        # render_template('login.html', title = "Login", baseForm = BaseForm(), loginForm = LoginForm())

@bp.route('/')
@require_login
def index():
    """Main page route."""

    return render_template('index.html', title = "tranxxeno lab collectanea")

@bp.route('/tag/<tag_name>', methods=['GET'])
@bp.route('/tag/<tag_name>/<int:count>', methods=['GET'])
@require_login
def view_by_tag(tag_name, count=30):
    #results = DB.find_text("items", "'{}'".format(tag_name), limit=count)
    results = DB.findByTag("items", tag_name, limit = count, sort = [("date_submitted", pymongo.DESCENDING)])

    results_new = []
    for result in results:
        result["_id"] = str(result["_id"])
        results_new.append(result)

    results = results_new

    return render_template('view_by_tag.html', title = "{} || tranxxeno lab collectanea".format(tag_name), data = results, tag_name = tag_name)
    #return render_template('index.html', title = "tranxxeno lab collectanea")

@bp.route('/view/<item_type>', methods=['GET'])
@bp.route('/view/<item_type>/<int:count>', methods=['GET'])
@require_login
def view_items(item_type, count = 30):
    if (item_type not in item_types):
        category = False
        # Assume that it is an object ID
        results = DB.find("items", {"_id": ObjectId(item_type)}, limit = 1)
        item_type = results[0]["type"]
    else:
        category = True
        if (item_type == "all"):
            results = DB.find("items", {}, limit = count, sort = [("date_modified", pymongo.DESCENDING)])
        else:
            results = DB.find("items", {"type": item_type}, limit = count, sort = [("date_modified", pymongo.DESCENDING)])

    results_new = []
    for result in results:
        result["_id"] = str(result["_id"])
        results_new.append(result)

    results = results_new
    #print(results)

    if (item_type == "image"):
        return render_template('view_image.html', title = "{} || tranxxeno lab collectanea".format(item_type), data = results, item_type = item_type, category = category)
    elif (item_type == "video"):
        return render_template('view_video.html', title = "{} || tranxxeno lab collectanea".format(item_type), data = results, item_type = item_type, category = category)
    else:
        return render_template('view_items.html', title = "{} || tranxxeno lab collectanea".format(item_type), data = results, item_type = item_type, category = category)

@bp.route('/json/item/<item_id>', methods=['GET'])
@require_login
def json_items_item(item_id):
    # Assume that it is an object ID
    results = DB.find("items", {"_id": ObjectId(item_id)}, limit = 1)
    j = []
    for item in results:
        item["_id"] = str(item["_id"])
        item["date_submitted"] = str(item["date_submitted"])
        item["date_modified"] = str(item["date_modified"])
        j.append(item)

    return json.dumps(j)


@bp.route('/json/type/<item_type>', methods=['GET'])
@require_login
def json_items_type(item_type):
    if (item_type not in item_types):
       return json.dumps([]) 
    else:
        #results = DB.find("items", {"type": item_type})
        results = DB.find("items", {"type": item_type}, sort = [("date_submitted", pymongo.DESCENDING)])
        j = []
        for item in results:
            item["_id"] = str(item["_id"])
            item["date_submitted"] = str(item["date_submitted"])
            item["date_modified"] = str(item["date_modified"])
            j.append(item)
    return json.dumps(j)


@bp.route('/json/tag/<tag_name>', methods=['GET'])
@bp.route('/json/tag/<tag_name>/<int:count>', methods=['GET'])
@require_login
def json_items_tag(tag_name, count = 100):
    results = DB.findByTag("items", tag_name, limit = count, sort = [("date_submitted", pymongo.DESCENDING)])
    j = []
    for item in results:
        item["_id"] = str(item["_id"])
        item["date_submitted"] = str(item["date_submitted"])
        item["date_modified"] = str(item["date_modified"])
        j.append(item)
    return json.dumps(j)


@bp.route('/add_item', methods=['GET', 'POST'])
@require_login
def add_item():
    message = []
    success = None

    # TODO deal with validators, validate_on_submit, and require data later
    if (request.method == "POST"):
        data = {}
        data["date_submitted"] = datetime.datetime.utcnow()
        data["date_modified"] = data["date_submitted"]
        data["type"] = request.form["itemType"]
        if (request.form["itemType"] == "note"):
            data["title"] = request.form["noteTitle"]
            data["entry"] = request.form["noteEntry"]
            if (request.form["noteSource"] != ""):
                data["source"] = request.form["noteSource"]

            data["tags"] = request.form.getlist("tags")
            result = DB.insert("items", data)
            if result:
                success = True
            else:
                success = False
        elif (request.form["itemType"] == "experiment"):
            data["title"] = request.form["experimentTitle"]
            data["entry"] = request.form["experimentEntry"]
            data["source"] = request.form["experimentSource"]
            data["tags"] = request.form.getlist("tags")
            result = DB.insert("items", data)
            if result:
                success = True
            else:
                success = False
        elif (request.form["itemType"] == "protocol"):
            data["title"] = request.form["protocolTitle"]
            data["entry"] = request.form["protocolEntry"]
            data["source"] = request.form["protocolSource"]
            data["tags"] = request.form.getlist("tags")
            result = DB.insert("items", data)
            if result:
                success = True
            else:
                success = False

        elif (request.form["itemType"] == "image"):
            f = request.files["image"]

            if (f.filename != ''):
                uuidPrefix = uuid.uuid4().hex[0:8]
                try:
                    response = linode_client.upload_fileobj(f, BUCKET, "images/" + uuidPrefix + "-" + f.filename)
                except ClientError as e:
                    print(e)
                    return False
                # Adding a segment of a UUID to "ensure" a unique filename
                data["URL"] = BUCKET_URL + "/images/" + uuidPrefix + "-" + f.filename
                data["originalFilename"] = f.filename
                data["uniqueFilename"] = uuidPrefix + "-" + f.filename
                data["downloaded"] = True
            else:
                data["downloaded"] = False

            data["description"] = request.form["imageDescription"]
            data["source"] = request.form["imageSourceURL"]
            data["sourcePage"] = request.form["imageSourcePage"]
            data["tags"] = request.form.getlist("tags")
    
            result = DB.insert("items", data)
            if result:
                success = True
            else:
                success = False
        elif (request.form["itemType"] == "video"):
            f = request.files["video"]
            if (f.filename != ''):
                uuidPrefix = uuid.uuid4().hex[0:8]
                try:
                    response = linode_client.upload_fileobj(f, BUCKET, "videos/" + uuidPrefix + "-" + f.filename)
                except ClientError as e:
                    print(e)
                    return False
                data["originalFilename"] = f.filename
                data["uniqueFilename"] = uuidPrefix + "-" + f.filename
                data["downloaded"] = True
                data["URL"] = BUCKET_URL + "/videos/" + uuidPrefix + "-" + f.filename
            else:
                data["downloaded"] = False

            data["description"] = request.form["videoDescription"]
            data["source"] = request.form["videoSource"]
            data["tags"] = request.form.getlist("tags")
            # Adding a segment of a UUID to "ensure" a unique filename
    
            result = DB.insert("items", data)
            if result:
                success = True
            else:
                success = False
        elif (request.form["itemType"] == "sound"):
            f = request.files["sound"]
            uuidPrefix = uuid.uuid4().hex[0:8]
            try:
                response = linode_client.upload_fileobj(f, BUCKET, "sounds/" + uuidPrefix + "-" + f.filename)
            except ClientError as e:
                print(e)
                return False
            data["description"] = request.form["soundDescription"]
            data["source"] = request.form["soundSource"]
            data["tags"] = request.form.getlist("tags")
            # Adding a segment of a UUID to "ensure" a unique filename
            data["URL"] = BUCKET_URL + "/sounds/" + uuidPrefix + "-" + f.filename
            data["originalFilename"] = f.filename
            data["uniqueFilename"] = uuidPrefix + "-" + f.filename
    
            result = DB.insert("items", data)
            if result:
                success = True
            else:
                success = False

        elif (request.form["itemType"] == "file"):
            f = request.files["fileName"]
            uuidPrefix = uuid.uuid4().hex[0:8]
            try:
                response = linode_client.upload_fileobj(f, BUCKET, "files/" + uuidPrefix + "-" + f.filename)
            except ClientError as e:
                print(e)
                return False
            data["description"] = request.form["fileDescription"]
            data["source"] = request.form["fileSource"]
            data["tags"] = request.form.getlist("tags")
            # Adding a segment of a UUID to "ensure" a unique filename
            data["URL"] = BUCKET_URL + "/files/" + uuidPrefix + "-" + f.filename
            data["originalFilename"] = f.filename
            data["uniqueFilename"] = uuidPrefix + "-" + f.filename
    
            result = DB.insert("items", data)
            if result:
                success = True
            else:
                success = False
        elif (request.form["itemType"] == "webpage"):
            data["title"] = request.form["webpageTitle"]
            data["source"] = request.form["webpageSource"]
            data["tags"] = request.form.getlist("tags")
            data["downloaded"] = False
            result = DB.insert("items", data)
            if result:
                success = True
            else:
                success = False
        elif (request.form["itemType"] == "concept"):
            data["title"] = request.form["conceptTitle"]
            data["description"] = request.form["conceptDescription"]
            #data["ties"] = process_tags(request.form["conceptTies"])
            data["tags"] = request.form.getlist("tags")
            result = DB.insert("items", data)
            if result:
                success = True
            else:
                success = False
        elif (request.form["itemType"] == "temporality"):
            data["title"] = request.form["temporalityTitle"]
            data["description"] = request.form["temporalityDescription"]
            #data["ties"] = process_tags(request.form["temporalityTies"])
            data["tags"] = request.form.getlist("tags")
            result = DB.insert("items", data)
            if result:
                success = True
            else:
                success = False

        elif (request.form["itemType"] == "daily_tarot"):
            data["work"] = request.form["dailyTarotWork"]
            data["relationships"] = request.form["dailyTarotRelationships"]
            data["costar"] = request.form["dailyTarotCostar"]
            data["notes"] = request.form["dailyTarotNotes"]
            data["tags"] = request.form.getlist("tags")
            result = DB.insert("items", data)
            if result:
                success = True
            else:
                success = False
        elif (request.form["itemType"] == "cycles"):
            dt_now = datetime.datetime.utcnow()
            dt_entry = datetime.datetime.strptime(request.form["cyclesDate"], "%Y-%m-%d")
            data["date_submitted"] = datetime.datetime(
                    dt_entry.year,
                    dt_entry.month,
                    dt_entry.day,
                    dt_now.hour,
                    dt_now.minute,
                    dt_now.second,
                    dt_now.microsecond)
            data["date_modified"] = data["date_submitted"]
            data["moon"] = request.form["cyclesMoon"]
            data["patches"] = request.form["cyclesPatches"]
            data["notes"] = request.form["cyclesNotes"]
            data["tags"] = request.form.getlist("tags")
            result = DB.insert("items", data)
            if result:
                success = True
            else:
                success = False
        elif (request.form["itemType"] == "letters"):
            dt_now = datetime.datetime.utcnow()
            dt_entry = datetime.datetime.strptime(request.form["lettersDate"], "%Y-%m-%d")
            data["date_submitted"] = datetime.datetime(
                    dt_entry.year,
                    dt_entry.month,
                    dt_entry.day,
                    dt_now.hour,
                    dt_now.minute,
                    dt_now.second,
                    dt_now.microsecond)
            data["date_modified"] = data["date_submitted"]
            data["name"] = request.form["lettersName"]
            data["email"] = request.form["lettersEmail"]
            data["phone"] = request.form["lettersPhone"]
            data["address"] = request.form["lettersAddress"]
            data["letter"] = request.form["lettersLetter"]
            data["notes"] = request.form["lettersNotes"]
            data["tags"] = request.form.getlist("tags")
            result = DB.insert("items", data)
            if result:
                success = True
            else:
                success = False

        else:
            output = "Something strange happened"

        if (success == True):
            flash("Successfully added to the collectanea!", "success")
        elif (success == False):
            flash("Some sort of error adding to the database, try again", "danger")
        elif (success == None):
            flash("This shouldn't have happened, try again", "danger")


    try:
        passedItemType = request.args.get('itemType', '')
        baseForm = BaseForm()
        webpageItemForm = WebpageItemForm()
        videoItemForm = VideoItemForm()
        
        if (passedItemType != ''):
            baseForm.itemType.default = passedItemType
            baseForm.process()
            if (passedItemType == "webpage"):
                webpageItemForm.webpageTitle.data = request.args.get('title', '')
                webpageItemForm.webpageSource.data = request.args.get('url', '')
            elif (passedItemType == "video"):
                videoItemForm.videoDescription.data = request.args.get('title', '')
                videoItemForm.videoSource.data = request.args.get('url', '')

    except KeyError:
        baseForm = BaseForm()
        webpageItemForm = WebpageItemForm()
        videoItemForm = VideoItemForm()



    return render_template('add_item.html', title = "Add Item || tranxxeno lab collectanea", baseForm = baseForm, temporalityItemForm = TemporalityItemForm(), conceptItemForm = ConceptItemForm(), noteItemForm = NoteItemForm(), imageItemForm = ImageItemForm(), videoItemForm = videoItemForm, soundItemForm = SoundItemForm(), fileItemForm = FileItemForm(),experimentItemForm = ExperimentItemForm(), protocolItemForm = ProtocolItemForm(), webpageItemForm = webpageItemForm, dailyTarotItemForm = DailyTarotItemForm(), cyclesItemForm = CyclesItemForm(), lettersItemForm = LettersItemForm(), ties = ties, passedItemType = passedItemType)

    return render_template('submit.html', output = output)

@bp.route('/edit_item/<item_id>', methods=['GET', 'POST'])
@require_login
def edit_item(item_id):
    message = []
    success = None
    formMapping = {
        "cycles": CyclesItemForm(),
        "letters": LettersItemForm(),
        "daily_tarot": DailyTarotItemForm(),
        "concept": ConceptItemForm(),
        "temporality": TemporalityItemForm(),
        "note": NoteItemForm(),
        "image": ImageItemForm(),
        "video": VideoItemForm(),
        "sound": SoundItemForm(),
        "file": FileItemForm(),
        "protocol": ProtocolItemForm(),
        "experiment": ExperimentItemForm(),
        "webpage": WebpageItemForm(),
    }


    # TODO deal with validators, validate_on_submit, and require data later
    if (request.method == "POST"):
        # TODO deal with dates on cycles entry
        result = DB.findByID("items", ObjectId(item_id))
        itemType = result["type"]
        id_value = result["_id"]

        if (itemType == "daily_tarot"): itemType = "dailyTarot"

        #print(request.form)

        for key in request.form.keys():
            if (key not in [itemType + "Id", "tags", "csrf_token", "submit"]):
                #print(key.split(itemType))
                result_key = key.split(itemType)[1].lower()
                result[result_key] = request.form[key]
            elif (key == "tags"):
                #result[result_key] = process_tags(request.form[key])
                result["tags"] = request.form.getlist("tags")

        try:
            f = request.files["image"]
    
            if (f.filename != ''):
                print("HERE!!!! in image processing")
                uuidPrefix = uuid.uuid4().hex[0:8]
                try:
                    response = linode_client.upload_fileobj(f, BUCKET, "images/" + uuidPrefix + "-" + f.filename)
                except ClientError as e:
                    print(e)
                    return False
                # Adding a segment of a UUID to "ensure" a unique filename
                result["URL"] = BUCKET_URL + "/images/" + uuidPrefix + "-" + f.filename
                result["originalFilename"] = f.filename
                result["uniqueFilename"] = uuidPrefix + "-" + f.filename
                result["downloaded"] = True
        except KeyError:
            pass


        result["date_modified"] = datetime.datetime.utcnow()
        DB.update("items", str(id_value), result)
        flash("Successfully updated the entry!", "success")

        #return render_template('view_items.html', title = "{} || tranxxeno lab collectanea".format(itemType), data = result, item_type = id_value)

        return redirect(url_for("main.view_items", item_type = id_value))


    result = DB.findByID("items", ObjectId(item_id))
    
    # Convert results to the form needed to populate the appropriate form
    transformedResult = {}
    itemType = result["type"]
    baseForm = BaseForm()
    baseForm.itemType = itemType

    form = formMapping[result["type"]]
    if (itemType == "daily_tarot"): itemType = "dailyTarot"
    for key in result.keys():
        if key == "_id":
            v = getattr(form, itemType + "Id")
            v.data = result[key]
            #setattr(form, itemType + "Id", result[key])
            #form.data[itemType + "Id"] = result[key]
            transformedResult[itemType + "Id"] = result[key]
        elif (key == "date_submitted") and (itemType == "cycles"):
            # TODO Deal with date formatting
            pass
        elif (key == "date_submitted") or (key == "date_modified") or (key == "type") or (key == "downloaded") or (key == "originalFilename") or (key == "uniqueFilename") or (key == "URL") or (key == "tags"):
            continue
        else:
            #print(dir(form))
            #print(result[key])
            #print(itemType)
            #print(key)

            if (key == "source"): 
                try:
                    v = getattr(form, itemType + "Source")
                except (AttributeError):

                    try:
                        v = getattr(form, itemType + "SourceURL")
                    except (AttributeError):
                        fail = True

            elif (key == "sourcePage"):
                v = getattr(form, itemType + "SourcePage")
            else:
                try:
                    v = getattr(form, itemType + key.capitalize())
                except (AttributeError):
                    continue

            v.data = result[key]
            if (key == "tags"):
                data = result[key]
                if (type(data) is str):
                    v.data = data
                else:
                    v.data = ", ".join(result[key])
            #setattr(form, itemType + key.capitalize(), result[key])
            #form.data[itemType + key.capitalize()] = result[key]
            transformedResult[itemType + key.capitalize()] = result[key]

    id_value = result["_id"]
    #form = formMapping[result["type"]].populate_obj(transformedResult)
    #form = formMapping[result["type"]].populate_obj(transformedResult)

    return render_template('edit_item.html', title = "Edit Item {} || tranxxeno lab collectanea".format(id_value), id_value = id_value, baseForm = baseForm, form = form, tags = result["tags"])


@bp.route('/ajax_search/<search_term>', methods=['GET'])
@bp.route('/ajax_search/<field>/<search_term>', methods=['GET'])
@require_login
def ajax_search(search_term, field = None):
    if field is not None:
        return DB.find_text("items", search_term, projection = {"{}".format(field): 1})
    else:
        return DB.find_text("items", search_term)

@bp.route('/autocomplete/<field>/<search_term>', methods=['GET'])
@require_login
def autocomplete(search_term, field):
    results = json.loads(ajax_search(search_term, field))

    values = []
    """
    {
    // Query is not required as of version 1.2.5
    "query": "Unit",
    "suggestions": [
        { "value": "United Arab Emirates", "data": "AE" },
        { "value": "United Kingdom",       "data": "UK" },
        { "value": "United States",        "data": "US" }
    ]
}
"""
    for result in results:
        #print(result)
        if (field == "tags"):
            tags = result[field]
            for tag in tags:
                if (tag not in values):
                    values.append(tag)
        elif (field == "title"):
            #print(result)
            title = result[field]
            _id = result["_id"]
            values.append((_id, title))
        else:
            pass
    values.sort()
    #result = {"suggestions": []}
    result = []
    for value in values:
        #result["suggestions"].append({"value": value, "data": value})
        result.append({"value": value, "text": value})
    
    if (search_term not in result):
        result.insert(0, {"value": search_term, "text": search_term})
    result = jsonify(result)
    result.headers.add('Access-Control-Allow-Origin', '*')
    return result

@bp.route('/api', methods=['POST'])
@require_login
def api():
    r = request.get_json()
    query = r['query']
    return render_template('index.html')


@bp.route('/tokenize_test', methods=['GET', 'POST'])
@require_login
def tokenize_test():
    if (request.method == "POST"):
        print(request.form)
        print(request.form.getlist("tags"))
    return render_template('tokenize_test.html')


@bp.route('/login', methods=['POST', 'GET'])
def login():
    message = "Please login."
    
    form = LoginForm()
    if (form.validate_on_submit()):
        username = request.form.get('username')
        password = request.form.get('password')

        result = DB.find("users", {"username": username}, limit = 1)

        if result:
            if (bcrypt.checkpw(password.encode("UTF-8"), result[0]["password"])):
                flash("Successfully logged in!", "success")
                session["username"] = username
                return render_template('index.html', title = "tranxxeno lab collectanea")
        
        flash("Wrong username or password", "error")

    return render_template("login.html", title = "Login", baseForm = BaseForm(), loginForm = form)


@bp.route('/logout')
@require_login
def logout():
    try:
        session.pop("username")
        session.clear()
        flash("You have been logged out", "success")
    except KeyError:
        pass

    return render_template('login.html', title = "Login", baseForm = BaseForm(), loginForm = LoginForm())

def process_tags(tags):
    if (tags == ""): return None

    tag_list = []

    try:
        l = tags.split(",")
        for item in l:
            tag_list.append(item.strip())
        return tag_list
    except:
        if (len(tags) != 0):
            tag_list.append(tags)
        return tag_list
