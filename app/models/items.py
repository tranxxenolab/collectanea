import datetime, json

from flask_wtf import FlaskForm, CSRFProtect
from wtforms import StringField, SubmitField, BooleanField, PasswordField, IntegerField, FormField, SelectField, FieldList, HiddenField, FileField, DateField, PasswordField
from wtforms.validators import DataRequired, Length
from wtforms.fields import *

from app.database import DB

ITEM_TEXT = "text"
ITEM_IMAGE = "image"
ITEM_VIDEO = "video"
ITEM_SOUND = "sound"
ITEM_WEBPAGE = "webpage"
ITEM_EXPERIMENT = "experiment"
ITEM_PROTOCOL = "protocol"
ITEM_TAROT = "daily_tarot"
ITEM_CYCLES = "cycles"
ITEM_LETTERS = "letters"

# later, make this just a base class for specific kinds of items
class Item(object):
    def __init__(self):
        self.date_submitted = datetime.datetime.utcnow()

    def set_item_type(self, item_type = ITEM_TEXT):
        self.item_type = item_type

    def insert(self):
        # if not DB.find_one("items", {"name": self.name})
        DB.insert(collection = "items", data = self.json())

    def json(self):
        item = {
            "type": self.item_type,
            "date_submitted": self.date_submitted
        }
        return json.dumps(item)

class BaseForm(FlaskForm):
    itemType = SelectField("Item Type", choices = [('concept', 'Concept'),('temporality', 'Temporality'), ('note', 'Note'), ('image', 'Image'), ('video', 'Video'), ("sound", "Sound"), ("file", "File"), ("webpage", "Webpage"), ("experiment", "Experiment"), ("protocol", "Protocol"), ("daily_tarot", "Daily Tarot"), ("cycles", "Cycles Diary"),("letters", "Letters")], id = "item_type")
    tags = StringField("Tags", id = "tags_tokenize")
    submit = SubmitField("Submit")

class ConceptItemForm(FlaskForm):
    conceptId = HiddenField("ID")
    conceptTitle = StringField("Title")
    conceptDescription = TextAreaField("Description")

class TemporalityItemForm(FlaskForm):
    temporalityId = HiddenField("ID")
    temporalityTitle = StringField("Title")
    temporalityDescription = TextAreaField("Description")


class NoteItemForm(FlaskForm):
    noteId = HiddenField("ID")
    noteTitle = StringField("Title", id = "noteTitle")
    noteEntry = TextAreaField("Entry", id = "noteEntry", render_kw={"rows": 14})
    noteSource = StringField("Source", id = "noteSource")

class ImageItemForm(FlaskForm):
    # TODO deal with validators later
    #image = FileField(u'Image File', [validators.regexp(u'^[^/\\]\.{jpg, png, tiff, tif, gif}$')])
    imageId = HiddenField("ID")
    image = FileField(u'Image File')
    imageDescription  = TextAreaField(u'Image Description')
    imageSourcePage = StringField(u'Image Source (book, webpage, etc)')
    imageSourceURL = StringField(u'URL of the actual image')

class VideoItemForm(FlaskForm):
    # TODO deal with validators later
    #video = FileField(u'Video File', [validators.regexp(u'^[^/\\]\.{jpg, png, tiff, tif, gif}$')])
    videoId = HiddenField("ID")
    video = FileField(u'Video File')
    videoDescription  = StringField(u'Video Description')
    videoSource = StringField(u'Video Source (URL, book, etc)')

class SoundItemForm(FlaskForm):
    # TODO deal with validators later
    #sound = FileField(u'Sound File', [validators.regexp(u'^[^/\\]\.{jpg, png, tiff, tif, gif}$')])
    soundId = HiddenField("ID")
    sound = FileField(u'Sound File')
    soundDescription  = StringField(u'Sound Description')
    soundSource = StringField(u'Sound Source (URL, book, etc)')

class FileItemForm(FlaskForm):
    # TODO deal with validators later
    #file = FileField(u'File File', [validators.regexp(u'^[^/\\]\.{jpg, png, tiff, tif, gif}$')])
    fileId = HiddenField("ID")
    fileName = FileField(u'File File')
    fileDescription  = StringField(u'File Description')
    fileSource = StringField(u'File Source (URL, book, etc)')

class WebpageItemForm(FlaskForm):
    webpageId = HiddenField("ID")
    webpageTitle = StringField("Title")
    webpageSource = StringField(u'Webpage Source (URL, book, etc)')

class ProtocolItemForm(FlaskForm):
    protocolId = HiddenField("ID")
    protocolTitle = StringField("Title")
    protocolEntry = TextAreaField("Protocol", render_kw={"rows": 14})
    protocolSource = StringField(u'Protocol Source (URL, book, etc)')

class ExperimentItemForm(FlaskForm):
    experimentId = HiddenField("ID")
    experimentTitle = StringField("Title")
    experimentEntry = TextAreaField("Experiment", render_kw={"rows": 14})
    experimentSource = StringField(u'Experiment Source (URL, book, etc)')

class DailyTarotItemForm(FlaskForm):
    dailyTarotId = HiddenField("ID")
    dailyTarotWork = StringField("Work")
    dailyTarotRelationships = StringField("Relationships")
    dailyTarotCostar = StringField("Co-Star")
    dailyTarotNotes = TextAreaField("Notes", render_kw={"rows": 8})

class CyclesItemForm(FlaskForm):
    cyclesId = HiddenField("ID")
    cyclesDate = DateField("Entry Date (Y-M-d)")
    cyclesMoon = StringField("Moon")
    cyclesPatches = StringField("Patches")
    cyclesNotes = TextAreaField("Notes", render_kw={"rows": 8})

class LettersItemForm(FlaskForm):
    lettersId = HiddenField("ID")
    lettersDate = DateField("Letter writing date (Y-M-d)")
    lettersName = StringField("Name")
    lettersEmail = StringField("E-mail address")
    lettersPhone = StringField("Phone Number")
    lettersAddress = TextAreaField("Address")
    lettersLetter = TextAreaField("Letter", render_kw={"rows": 14})
    lettersNotes = TextAreaField("Notes")

class LoginForm(FlaskForm):
    username = StringField("Username", [DataRequired()], id = "username")
    password = PasswordField("Password", [DataRequired()], id = "password")

