#!python3
import datetime, json, pytz

tz = pytz.timezone('Europe/Amsterdam')

entries = []

with open("tarot.md") as f:
    while True:
        try:
            entry = {}
            entry["type"] = "daily_tarot"
            entry["tags"] = ["tarot", "Co-Star", "Next World Tarot", "Rider-Waite"] 
            date = f.readline()
            dates = date.split('.')
            dt = datetime.datetime(int("20" + dates[2]), int(dates[1]), int(dates[0].strip()), 10, 35, 35, tzinfo = tz)
            #entry["date_submitted"] = str(dt)
            #entry["date_submitted"] = dt.astimezone(datetime.timezone.utc).isoformat()
            entry["date_submitted"] = dt.isoformat()
            junk = f.readline()
            work = f.readline()
            work = (work.split(":")[1]).strip()
            entry["work"] = work
            relationships = f.readline()
            relationships = (relationships.split(":")[1]).strip()
            entry["relationships"] = relationships
            costar = f.readline()
            costar = (costar.split(":")[1]).strip()
            entry["costar"] = costar
            junk = f.readline()
            #print(dt, work, relationships, costar)
            entries.append(entry)
        except (IndexError):
            break

entries_json = json.dumps(entries)

with open("tarot.json", "w") as f:
    f.write(entries_json)
