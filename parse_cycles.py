#!python3
import datetime, json, pytz

tz = pytz.timezone('Europe/Amsterdam')

entries = []

with open("cycles.md") as f:
    while True:
        try:
            entry = {}
            entry["type"] = "cycles"
            entry["tags"] = ["moon", "patches"] 
            date = f.readline()
            dates = date.split('.')
            dt = datetime.datetime(int("20" + dates[2]), int(dates[1]), int(dates[0].strip()), 10, 35, 35, tzinfo = tz)
            #entry["date_submitted"] = str(dt)
            #entry["date_submitted"] = dt.astimezone(datetime.timezone.utc).isoformat()
            entry["date_submitted"] = dt.isoformat()
            junk = f.readline()
            moon = f.readline()
            moon = (moon.split(":")[1]).strip()
            entry["moon"] = moon
            patches = f.readline()
            patches = (patches.split(":")[1]).strip()
            entry["patches"] = patches
            junk = f.readline()
            notes = f.readline()
            notes = (notes.split(":")[1]).strip()
            entry["notes"] = notes
            print(dt, moon, patches, notes)
            entries.append(entry)
            junk = f.readline()
            junk = f.readline()
        except (IndexError):
            break

entries_json = json.dumps(entries)

with open("cycles.json", "w") as f:
    f.write(entries_json)
