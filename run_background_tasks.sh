#!/bin/bash

source ~/miniconda3/etc/profile.d/conda.sh
conda activate collectanea
cd /var/www/tranxxenolab.net/collectanea
python download_files.py
